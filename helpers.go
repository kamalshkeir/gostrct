package gostrct

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func SetFieldValue(fld reflect.Value, value any) {
	valueToSet := reflect.ValueOf(value)
	switch fld.Kind() {
	case valueToSet.Kind():
		fld.Set(valueToSet)
	case reflect.Ptr:
		unwrapped := fld.Elem()
		if !unwrapped.IsValid() {
			newUnwrapped := reflect.New(fld.Type().Elem())
			SetFieldValue(newUnwrapped, value)
			fld.Set(newUnwrapped)
			return
		}
		SetFieldValue(unwrapped, value)
	case reflect.Interface:
		unwrapped := fld.Elem()
		SetFieldValue(unwrapped, value)
	case reflect.Struct:
		switch v := value.(type) {
		case string:
			if strings.Contains(v, ":") || strings.Contains(v, "-") {
				l := len("2006-01-02T15:04")
				if strings.Contains(v[:l], "T") {
					if len(v) >= l {
						t, err := time.Parse("2006-01-02T15:04", v[:l])
						if err != nil {
							fld.Set(reflect.ValueOf(t))
						}
					}
				} else if len(v) >= len("2006-01-02 15:04:05") {
					t, err := time.Parse("2006-01-02 15:04:05", v[:len("2006-01-02 15:04:05")])
					if err == nil {
						fld.Set(reflect.ValueOf(t))
					}
				} else {
					fmt.Println("SetFieldValue Struct: doesn't match any case", v)
				}
			}
		case time.Time:
			fld.Set(valueToSet)
		case []any:
			// walk the fields
			for i := 0; i < fld.NumField(); i++ {
				SetFieldValue(fld.Field(i), v[i])
			}
		}
	case reflect.String:
		switch valueToSet.Kind() {
		case reflect.String:
			fld.SetString(valueToSet.String())
		case reflect.Struct:
			fld.SetString(valueToSet.String())
		default:
			if valueToSet.IsValid() {
				fld.Set(valueToSet)
			} else {
				fmt.Println("value", valueToSet.Interface(), "is not valid")
			}
		}
	case reflect.Int:
		switch v := value.(type) {
		case int64:
			fld.SetInt(v)
		case string:
			if v, err := strconv.Atoi(v); err == nil {
				fld.SetInt(int64(v))
			}
		case int:
			fld.SetInt(int64(v))
		}
	case reflect.Int64:
		switch v := value.(type) {
		case int64:
			fld.SetInt(v)
		case string:
			if v, err := strconv.Atoi(v); err == nil {
				fld.SetInt(int64(v))
			}
		case []byte:
			if v, err := strconv.Atoi(string(v)); err != nil {
				fld.SetInt(int64(v))
			}
		case int:
			fld.SetInt(int64(v))
		}
	case reflect.Bool:
		switch valueToSet.Kind() {
		case reflect.Int:
			if value == 1 {
				fld.SetBool(true)
			}
		case reflect.Int64:
			if value == int64(1) {
				fld.SetBool(true)
			}
		case reflect.Uint64:
			if value == uint64(1) {
				fld.SetBool(true)
			}
		case reflect.String:
			if value == "1" {
				fld.SetBool(true)
			} else if value == "true" {
				fld.SetBool(true)
			}
		}
	case reflect.Uint:
		switch v := value.(type) {
		case uint:
			fld.SetUint(uint64(v))
		case uint64:
			fld.SetUint(v)
		case int64:
			fld.SetUint(uint64(v))
		case int:
			fld.SetUint(uint64(v))
		}
	case reflect.Uint64:
		switch v := value.(type) {
		case uint:
			fld.SetUint(uint64(v))
		case uint64:
			fld.SetUint(v)
		case int64:
			fld.SetUint(uint64(v))
		case int:
			fld.SetUint(uint64(v))
		}
	case reflect.Float64:
		if v, ok := value.(float64); ok {
			fld.SetFloat(v)
		}
	case reflect.Slice:
		targetType := fld.Type()
		typeName := targetType.String()
		if strings.HasPrefix(typeName, "[]") {
			array := reflect.New(targetType).Elem()
			for _, v := range strings.Split(valueToSet.String(), ",") {
				array = reflect.Append(array, reflect.ValueOf(v))
			}
			fld.Set(array)
		}
	default:
		switch v := value.(type) {
		case []byte:
			fld.SetString(string(v))
		default:
			fmt.Println("setFieldValue: case not handled , unable to fill struct,field kind:", fld.Kind(), ",value to fill:", value)
		}
	}
}

func ToSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

func SnakeCaseToTitle(inputUnderScoreStr string) (camelCase string) {
	//snake_case to camelCase
	isToUpper := false
	for k, v := range inputUnderScoreStr {
		if k == 0 {
			camelCase = strings.ToUpper(string(inputUnderScoreStr[0]))
		} else {
			if isToUpper {
				camelCase += strings.ToUpper(string(v))
				isToUpper = false
			} else {
				if v == '_' {
					isToUpper = true
				} else {
					camelCase += string(v)
				}
			}
		}
	}
	return
}

func GetStructInfos[T comparable](strct *T,tags ...string) (fields []string, fValues map[string]any, fTypes map[string]string, fTags map[string][]string) {
	fields = []string{}
	fValues = map[string]any{}
	fTypes = map[string]string{}
	fTags = map[string][]string{}

	s := reflect.ValueOf(strct).Elem()
	typeOfT := s.Type()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		fname := typeOfT.Field(i).Name
		fname = ToSnakeCase(fname)
		fvalue := f.Interface()
		ftype := f.Type().Name()

		fields = append(fields, fname)
		fTypes[fname] = ftype
		fValues[fname] = fvalue
		for _,t := range tags {
			if ftag, ok := typeOfT.Field(i).Tag.Lookup(t); ok {
				tags := strings.Split(ftag, ";")
				fTags[fname] = append(fTags[fname], tags...)
			}
		}
	}
	return fields, fValues, fTypes, fTags
}
