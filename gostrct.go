package gostrct

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func FillValues(struct_to_fill any, values_to_fill ...any) {
	rs := reflect.ValueOf(struct_to_fill)
	if rs.Kind() == reflect.Pointer {
		rs = reflect.ValueOf(struct_to_fill).Elem()
	}
	typeOfT := rs.Type()
	for i := 0; i < rs.NumField(); i++ {
		if len(values_to_fill) < rs.NumField() && i == 0 {
			if strings.Contains(ToSnakeCase(typeOfT.Field(i).Name),"id") {
				continue
			} 
		} 
		field := rs.Field(i)
		if field.IsValid() {
			index := i
			if len(values_to_fill) < rs.NumField() {
				index= i-1
			}
			SetFieldValue(field, values_to_fill[index])
		}
	}
}

func FillSelectedValues(struct_to_fill any, fields_comma_separated string, values_to_fill ...any) {
	cols := strings.Split(fields_comma_separated, ",")
	if len(values_to_fill) != len(cols) {
		fmt.Println("error FillSelectedValues: len(values_to_fill) and len(struct fields) should be the same", len(values_to_fill), len(cols))
		return
	}
	rs := reflect.ValueOf(struct_to_fill)
	if rs.Kind() == reflect.Pointer {
		rs = reflect.ValueOf(struct_to_fill).Elem()
	}

	for i, col := range cols {
		var fieldToUpdate *reflect.Value
		if f := rs.FieldByName(SnakeCaseToTitle(col)); f.IsValid() && f.CanSet() {
			fieldToUpdate = &f
		} else if f := rs.FieldByName(col); f.IsValid() && f.CanSet() {
			// usually here
			fieldToUpdate = &f
		} else if f := rs.FieldByName(ToSnakeCase(col)); f.IsValid() && f.CanSet() {
			fieldToUpdate = &f
		}

		if fieldToUpdate.IsValid() {
			SetFieldValue(*fieldToUpdate, values_to_fill[i])
		}
	}
}

